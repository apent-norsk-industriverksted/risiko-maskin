# Risikoanalyse for maskiner

Leverandører av programvare har ofte to mål: Selge lisenser og kurs. Brukere har ofte litt andre interesser. For eksempel kvalitet og effektivitet.

Målet med dette prosjektet er å oppnå et godt grunnlag for risikoanalyse på maskiner. Maler og digitale sjekklister kan hjelpe til med å øke både kvalitet og effektivitet.

## Mal - CEDOC

Svenske Cedoc AB leverer programmvaren CEDOC. CEDOC er et verktøy som hjelper til med å belyse kravene i maskindirektivet.

Programmvaren produserer dette:

- Oversikt over dokumenter og ansvarsfordeling
- Detaljert analyserapport for kravene i maskinforskriften
- Tiltaksplan
- Forslag til samsvarserklæring
- Forslag til CE-merke

<https://cedoc.se/>

Malen **template V3_2_1 no.ced** gir liten merverdi i prosjekter. Nå kommer CEDOC V4 med et annet filformat også.

## Mal - SISTEMA

Den statlige tyske organisasjonen IFA har utviklet programmvaren SISTEMA. Sistema kan brukes som et hjelpemiddel for å konstruere industrielle sikkerhetssystemer.

<https://www.dguv.de/ifa/praxishilfen/practical-solutions-machine-safety/software-sistema/index.jsp>

Eksempelprosjektet **QB MR-06 SISTEMA eksempelprosjekt.ssm** brukes i kurset MR-06 på <https://kurs.lts.no>.

Den første sikkerhetsfunksjonen er fylt opp med referanser til ISO 13849-1:2023, og kan brukes som mal. Det er det et greit alternativ frem til SISTEMA 3 kommer med støtte for ISO 13849-1:2023.

## Sjekklister

Det ligger ingen sjekklister her. Og det er uklart hvordan en digital sjekkliste bør formateres. På tross av tette kjennes sjekklister så viktig at de har fått et eget kapittel. Legg gjerne inn et forslag.
