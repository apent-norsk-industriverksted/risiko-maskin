# Kursnotater

Vær oppmerksom på at kursmateriale sjelden er kompatibelt med MIT lisensen. Sørg for å få skriftlig samtykke fra de som har laget kurset før du publiserer kursnotater.
