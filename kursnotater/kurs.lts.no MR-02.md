# MR-02 Innkjøp, drift og vedlikehold av maskiner

Her er en samling av referansene som er brukt i kurset.

https://lts.no/tjenester/kurs

## Introduksjon

- [1] Forskrift om maskiner https://lovdata.no/dokument/SF/forskrift/2009-05-20-544
- [2] Maskinforordningen https://eur-lex.europa.eu/eli/reg/2023/1230/oj
- [3] Arbeidsmiljøloven § 5-5 https://lovdata.no/dokument/NL/lov/2005-06-17-62
- [4] Arbeidsmiljøloven § 4-4 tredje ledd https://lovdata.no/dokument/NL/lov/2005-06-17-62
- [5] FOR-1996-12-06-1127 Internkontrollforskriften https://lovdata.no/dokument/SF/forskrift/1996-12-06-1127
- [6] FOR-2011-12-06-1357 Forskrift om utførelse av arbeid https://lovdata.no/dokument/SF/forskrift/2011-12-06-1357
- [7] Arbeidstilsynet. (2024). Arbeidsulykker i industrien (Kompass tema nr. 1 2024).

## Maskinens grenser

- [1] Forskrift om maskiner - Vedlegg 1 - 1.7.4 Bruksanvisning
- [2] Forskrift om maskiner - Vedlegg 1 - 2.2.1.1 Vibrasjon fra håndstyrte maskiner - bruksanvisning
- [3] Forskrift om maskiner - Vedlegg 1 - 2.2.2.2 Boltepistoler etc. - bruksanvisning
- [4] Forskrift om maskiner - Vedlegg 1 - 3.2.1 Bevegelig maskin - førerplass - bruksanvisning
- [5] Forskrift om maskiner - Vedlegg 1 - 3.6.1 Bevegelig maskin - merking og skilting - bruksanvisning
- [6] Forskrift om maskiner - Vedlegg 1 - 2.4.10 Sprøytemiddelmaskiner - bruksanvisning
- [7] Forskrift om maskiner - Vedlegg 1 - 4.4 Tilleggskrav for løfteoperasjoner - bruksanvisning
- [8] EN ISO 20607:2019 - Safety of machinery - Instruction handbook - General drafting principles
- [9] Forskrift om maskiner - Vedlegg 1 - 1.5 Risiko med årsak i andre farer
- [10] EN 60204–1:2018  Maskinsikkerhet – Elektrisk utstyr på maskiner – Del 1 Generelle krav - Kapittel 17 Teknisk dokumentasjon
- [11] EN ISO 4413:2010 Hydraulic fluid power - General rules and safety requirements for systems - and their components - Kapittel 7 Informasjon for bruk
- [12] ISO 4413:2010 Pneumatic fluid power - General rules and safety requirements for systems and their components - Kapittel 7 Informasjon for bruk
- [13] Forskrift om maskiner - vedlegg I 1.1.3 Materialer og produkter

## Drift og vedlikeholdsarbeid

- [1] FOR-1996-12-06-1127 Internkontrollforskriften
- [2] FOR-2011-12-06-1357 Forskrift om utførelse av arbeid
- [3] https://www.arbeidstilsynet.no/risikofylt-arbeid/maskiner/sikkert-vedlikehold/
- [4] https://www.arbeidstilsynet.no/risikofylt-arbeid/maskiner/las-og-merk/

## Samling av maskiner

- [1] Forskrift om maskiner § 2.a, strekpunkt 4
- [2] Maskindirektivet artikkel 2a, strekpunkt 4
- [3] European Commission. (2024). Guide to application of the Machinery Directive 2006/42/EC (Edition 2.3) §38 https://ec.europa.eu/docsroom/documents/60145

## Eldre maskiner

- [1] Arbeidsmiljøloven § 4-4.Krav til det fysiske arbeidsmiljøet
- [2] Arbeidsmiljøloven § 5-5.Produsenter, leverandører og importører av maskiner og annet arbeidsutstyr
- [3] German Federal Ministry of Labour and Social Affairs. (2015, September). Interpretation paper on substantial modification to machinery. https://www.bmas.de/SharedDocs/Downloads/DE/Arbeitsschutz/en-interpretation-paper-substantial-modification-to-machinery.html
- [4] EN ISO 12100 Safety of machinery – General principles for design – Risk assessment and risk reduction
- [5] Regulation (EU) 2023/2013 of the European Parliament and of the Council of 14 June 2023
- [6] European Commission. (2024). Guide to application of the Machinery Directive 2006/42/EC (Edition 2.3) §39 https://ec.europa.eu/docsroom/documents/60145

## Kjøp av maskiner

- [1] FOR-2009-05-20-544 Forskrift om maskiner, Vedlegg I, 1.7.4.1
- [2] https://europa.eu/youreurope/business/product-requirements/compliance/technical-documentation-conformity/index_en.htm
- [3] Forskrift om maskiner, vedlegg II
- [4] Arbeidstilsynet. Produksjon og salg av maskiner. Retrieved 11 November 2024, from https://www.arbeidstilsynet.no/risikofylt-arbeid/maskiner/produksjon-og-salg-av-maskiner/
- [5] Forskrift om maskiner, vedlegg I del 2
- [6] Forskrift om maskiner, vedlegg VII
- [7] European Commission. (2024). Guide to application of the Machinery Directive 2006/42/EC (Edition 2.3) §81 https://ec.europa.eu/docsroom/documents/60145
